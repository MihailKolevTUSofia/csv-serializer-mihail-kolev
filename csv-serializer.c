#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct
{
    int row_id;
    int order_id;
    int order_date;
    int customer_id;
    int city;
    int state;
    int postal_code;
    int region;
    int product_id;
    int category;
    int sub_category;
    float price;
} SalesData;

int lookup(char *str, char ***lookup_table, int *occupied, int *size)
{
    for (int i = 0; i < *occupied; i++)
    {
        if (!strcmp(str, (*lookup_table)[i]))
        {
            return i;
        }
    }
    if (*size == *occupied)
    {
        *size = 2 * (*size);
        *lookup_table = realloc(*lookup_table, sizeof(char *) * (*size));
    }
    int len = strlen(str);
    (*lookup_table)[*occupied] = malloc(sizeof(char) * (len + 2));
    strcpy((*lookup_table)[*occupied], str);

    (*occupied)++;
    return *occupied - 1;
}

void write_string(char *str, FILE *file)
{
    int len = strlen(str);
    fwrite(&len, sizeof(int), 1, file);
    fwrite(str, sizeof(char), strlen(str), file);
}

void write_table(FILE *file, char **lookup_table, int entry_count)
{
    fwrite(&entry_count, sizeof(entry_count), 1, file);
    for (int i = 0; i < entry_count; ++i)
    {
        write_string(lookup_table[i], file);
    }
}

char *read_string(FILE *file)
{
    char *res = NULL;
    int size;
    fread(&size, sizeof(size), 1, file);
    res = malloc(sizeof(char) * (size + 1));
    fread(res, sizeof(char), size, file);
    res[size] = '\0';
    return res;
}

void read_table(FILE *file, char ***lookup_table, int *occupied)
{
    fread(occupied, sizeof(*occupied), 1, file);
    *lookup_table = malloc(sizeof(char *) * (*occupied));
    for (int i = 0; i < *occupied; i++)
    {
        (*lookup_table)[i] = read_string(file);
    }
}

SalesData parseLine(char *line, char ***lookup_table, int *occupied, int *size)
{
    SalesData sales;
    char *ptr = strtok(line, ",");
    int lineArgCounter = 0;
    while (ptr != NULL)
    {
        int index;
        if (lineArgCounter != 0 && lineArgCounter != 11)
        {
            index = lookup(ptr, lookup_table, occupied, size);
        }
        switch (lineArgCounter)
        {
        case 0:
            sales.row_id = atoi(ptr);
            break;
        case 1:
            sales.order_id = index;
            break;
        case 2:
            sales.order_date = index;
            break;
        case 3:
            sales.customer_id = index;
            break;
        case 4:
            sales.city = index;
            break;
        case 5:
            sales.state = index;
            break;
        case 6:
            sales.postal_code = index;
            break;
        case 7:
            sales.region = index;
            break;
        case 8:
            sales.product_id = index;
            break;
        case 9:
            sales.category = index;
            break;
        case 10:
            sales.sub_category = index;
            break;
        case 11:
            sales.price = atof(ptr);
            break;
        default:
            break;
        }
        lineArgCounter++;
        ptr = strtok(NULL, ",");
    }
    return sales;
}

int main(int argc, char *argv[])
{
    int flagSilent = 0;
    char *command = NULL;
    char *filename = NULL;
    int opt;

    while ((opt = getopt(argc, argv, "s")) != -1)
    {
        switch (opt)
        {
        case 's':
            flagSilent = 1;
            break;
        case '?':
            printf("Unknown option `-%c'.\n", optopt);
            return 1;
        default:
            return 1;
        }
    }

    if (argc - optind != 2)
    {
        printf("Usage: %s [-s] <command> <filename>\n", argv[0]);
        return 1;
    }
    command = argv[optind];
    filename = argv[optind + 1];
    char **lookup_table = NULL;
    int loccupied = 0;
    int lsize = 0;
    lookup_table = malloc(sizeof(char *));
    lsize = 1;

    if (strcmp(command, "serialize") == 0)
    {
        FILE *csv_file = fopen(filename, "r");
        if (csv_file == NULL)
        {
            if (flagSilent == 0)
            {
                printf("Error opening csv file.\n");
            }
            return 1;
        }

        SalesData *sales_data = malloc(sizeof(SalesData));
        int i = 0;
        int capacity = 1;
        char line[1024];
        while (fgets(line, sizeof(line), csv_file))
        {
            if (i == 0)
            {
                i++;
                continue;
            }
            sales_data[i - 1] = parseLine(line, &lookup_table, &loccupied, &lsize);
            i++;
            if (capacity <= i)
            {
                capacity *= 2;
                sales_data = realloc(sales_data, capacity * sizeof(SalesData));
            }
        }
        fclose(csv_file);

        FILE *bin_file = fopen("sales_data.bin", "wb");
        if (bin_file == NULL)
        {
            if (flagSilent == 0)
            {
                printf("Error creating binary file.\n");
            }
            free(sales_data);
            for (int i = 0; i < loccupied; i++)
            {
                free(lookup_table[i]);
            }
            free(lookup_table);
            return 1;
        }

        write_table(bin_file, lookup_table, loccupied);

        fwrite(sales_data, sizeof(SalesData), i - 1, bin_file);
        fclose(bin_file);
        free(sales_data);
        for (int i = 0; i < loccupied; i++)
        {
            free(lookup_table[i]);
        }
        free(lookup_table);

        if (flagSilent == 0)
        {
            printf("Serialized data written to sales_data.bin\n");
        }
    }
    else if (strcmp(command, "deserialize") == 0)
    {
        FILE *bin_file = fopen(filename, "rb");
        if (bin_file == NULL)
        {
            if (flagSilent == 0)
            {
                printf("Error opening binary file.\n");
            }
            return 1;
        }

        read_table(bin_file, &lookup_table, &loccupied);

        SalesData *sales_data = malloc(sizeof(SalesData));
        int size = 1;
        int occupied = 0;
        while (fread(sales_data + occupied, sizeof(SalesData), 1, bin_file))
        {
            ++occupied;
            if (size == occupied)
            {
                size *= 2;
                sales_data = realloc(sales_data, size * sizeof(SalesData));
            }
        }
        fclose(bin_file);

        FILE *csv_file = fopen("sales_new.csv", "w");
        if (csv_file == NULL)
        {
            if (flagSilent == 0)
            {
                printf("Error creating csv file.\n");
            }
            free(sales_data);
            for (int i = 0; i < loccupied; i++)
            {
                free(lookup_table[i]);
            }
            free(lookup_table);

            return 1;
        }

        fprintf(csv_file, "\"Row ID\",\"Order ID\",\"Order Date\",\"Customer ID\",\"City\",\"State\",\"Postal Code\",\"Region\",\"Product ID\",\"Category\",\"Sub-Category\",\"Price\"\n");
        for (int i = 0; i < occupied; i++)
        {
            fprintf(csv_file, "%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f\n",
                    i + 1,
                    lookup_table[sales_data[i].order_id],
                    lookup_table[sales_data[i].order_date],
                    lookup_table[sales_data[i].customer_id],
                    lookup_table[sales_data[i].city],
                    lookup_table[sales_data[i].state],
                    lookup_table[sales_data[i].postal_code],
                    lookup_table[sales_data[i].region],
                    lookup_table[sales_data[i].product_id],
                    lookup_table[sales_data[i].category],
                    lookup_table[sales_data[i].sub_category],
                    sales_data[i].price);
        }

        fclose(csv_file);
        free(sales_data);
        for (int i = 0; i < loccupied; i++)
        {
            free(lookup_table[i]);
        }
        free(lookup_table);

        if (flagSilent == 0)
        {
            printf("Deserialized data written to sales_new.csv\n");
        }
    }
    else
    {
        if (flagSilent == 0)
        {
            printf("Unknown command: %s\n", command);
        }
        return 1;
    }

    return 0;
}
